(() => {
    /**
     * Check and set a global guard variable.
     * If this content script is injected into the same page again,
     * it will do nothing next time.
     */
    if (window.hasRun) {
        return;
    }
    window.hasRun = true;

    function does_something(){
        console.log("ready to make an extension !!"); 
    }
    

    /**
     * Listen to popup message
     */
    browser.runtime.onMessage.addListener((message) => {

        // if it get that message!
        if (message.command === "message_name") {
            does_something();  
        }
        
    });

})();